package example.util;

import java.awt.FlowLayout;

import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * Collection of helper methods
 *
 */
public class CommonsUtil {

    /**
     * Returns <code>true</code> if a CharSequence is whitespace,
     * empty ("") or null, <code>false</code> otherwise.
     *
     * @param cs the sequence to check.
     * @return See above.
     */
    public static boolean isBlank(final CharSequence cs)
    {
        int strLen;
        if (cs == null || (strLen = cs.length()) == 0)
            return true;
        for (int i = 0; i < strLen; i++) {
            if (Character.isWhitespace(cs.charAt(i)) == false) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Returns <code>true</code> if the extension match, <code>false</code>
     * otherwise.
     * 
     * @param name The file name to extract the extension from.
     * @param extension The extension.
     * @return See above.
     */
    public static boolean isExtension(final String name, String extension) {
    	final String fileExtension = getFileExtension(name);
    	if (isBlank(fileExtension) || isBlank(extension)) {
    		return false;
    	}
    	return fileExtension.equals(extension);
    }
    
    /**
     * Returns the extension of the specified file name.
     * @param name The name to handle
     * @return See above.
     */
    public static String getFileExtension(final String name) {
    	if (name.contains(".")) {
    		return name.substring(name.lastIndexOf(".")+1);
    	}
    	return "";
    }
    
    public static String getFileNameNoExtension(final String name) {
    	if (name.contains(".")) {
    		return name.substring(0, name.lastIndexOf(".")-1);
    	}
    	return name;
    }
    
    /**
     * Adds the specified {@link JComponent} to a {@link JPanel} 
     * with a left flow layout.
     * 
     * @param component The component to add.
     * @param hgap    	The horizontal gap between components and between the 
     * 					components and the borders of the 
     * 					<code>Container</code>.
     * @param vgap    	The vertical gap between components and between the 
     * 					components and the borders of the 
     * 					<code>Container</code>.
     * @param isOpaque  Pass <code>true</code> if this component should be 
     * 					opaque, <code>false</code> otherwise.
     * @return See below.
     */
    public static JPanel buildComponentPanel(JComponent component, 
			int hgap, int vgap, boolean isOpaque)
    {
    	JPanel p = new JPanel();
    	if (component == null) return p;
    	if (hgap < 0) hgap = 0;
    	if (vgap < 0) vgap = 0;
    	p.setLayout(new FlowLayout(FlowLayout.RIGHT, hgap, vgap));
    	p.add(component);
    	p.setOpaque(isOpaque);
    	return p;
    }
}
