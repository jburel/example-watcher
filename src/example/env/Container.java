package example.env;

import java.io.File;

import example.env.config.Registry;
import example.env.config.RegistryFactory;
import example.env.init.Initializer;
import example.util.CommonsUtil;

public class Container {

	/** 
	 * Points to the configuration directory.
	 * The path is relative to the installation directory.
	 */
	public static final String CONFIG_DIR = "config";
	
	/** The name of the container's configuration file. */
	public static final String CONFIG_FILE = "config.xml";
	
	/**
	 * The sole instance.
	 */
	private static Container singleton;
	
	/**
	 * Returns the singleton instance.
	 * 
	 * @return	See above.
	 */
	static Container getInstance()
	{
		return singleton;
	}
	
	/**
	 * Performs the start up procedure.
	 * 
	 * @param home	Path to the installation directory.  If <code>null<code> or
	 * 				empty, then the user directory is assumed.
	 * @param configFile The configuration file.
	 */
	private static void runStartupProcedure(String home, String configFile)
	{
		AbnormalExitHandler.configure();
		Initializer initManager = null;
		try {
			singleton = new Container(home, configFile);
			initManager = new Initializer(singleton);
			initManager.configure();
			initManager.doInit();

			//startService() called by Initializer at end of doInit().
		} catch (Exception e) {
			if (initManager != null) initManager.rollback();
			AbnormalExitHandler.terminate(e);
		} 
		//Any other exception will be handled automatically by
		//AbnormalExitHandler.  In this case, we don't rollback,
		//as something completely unforeseen happened, so it's 
		//better not to make assumptions on the state of the
		//initialization manager.
	}
	
	/** The configuration file. */
	private String		configFile;
	
	/** Absolute path to the installation directory. */
	private String		homeDir;
	
	/** The container's registry. */
	private Registry	registry;
	
	/** 
	 * Initializes the member fields. 
	 * <p>The absolute path to the installation directory is obtained from
	 * <code>home</code>.  If this parameter doesn't specify an absolute path,
	 * then it'll be translated into an absolute path.  Translation is system 
	 * dependent -- in many cases, the path is resolved against the user 
	 * directory (typically the directory in which the JVM was invoked).</p>
	 * 
	 * @param home	Path to the installation directory.  If <code>null</code> or
	 * 				empty, then the user directory is assumed.
	 * @param configFile The configuration file.
	 * @throws StartupException	If <code>home</code> can't be resolved to a
	 * 			valid and existing directory. 				
	 */
	private Container(String home, String configFile)
		throws Exception
	{
	    if (CommonsUtil.isBlank(configFile) || !CommonsUtil.isExtension(configFile, "xml")) {
			configFile = CONFIG_FILE;
		}
		this.configFile = configFile;

        if (CommonsUtil.isBlank(home)) {
        	home = System.getProperty("user.dir");
		}
        File f = new File(home);
		
		//Now make it absolute. If the original path wasn't absolute, then
		//translation is system dependent. 
		f = f.getAbsoluteFile();
		homeDir = f.getAbsolutePath();
		//Make sure that what we've got is a directory. 
		if (!f.exists() || !f.isDirectory())
			throw new Exception("Can't locate home dir: "+homeDir);
		registry = RegistryFactory.makeNew(this);
	}
	
	/**
	 * Starts up the process
	 * @param home
	 * @param configFile
	 */
	public static void startup(final String home, final String configFile)
	{
		if (singleton != null) {
			return;
		}
		ThreadGroup root = new RootThreadGroup();
		Runnable r = new Runnable() {
			public void run() { runStartupProcedure(home, configFile); }
		};
		Thread t = new Thread(root, r, "Initializer");
        t.start();
		
	}
	
	/**
	 * Returns the relative path to the container's configuration file.
	 * 
	 * @return	See above.
	 */
	public String getConfigFileRelative()
	{ 
        return getConfigFileRelative(configFile);
	}
	
	/**
	 * Returns the relative path to the container's configuration file.
	 * 
	 * @param file The configuration file.
	 * @return	See above.
	 */
	public String getConfigFileRelative(String file)
	{ 
		return getFileRelative(CONFIG_DIR, file);
	}
	
	/**
	 * Returns the relative path to the container's configuration file
	 * 
	 * @param file The configuration file.
	 * @return	See above.
	 */
	public String getFileRelative(String directory, String file)
	{ 
		return resolveFilePath(file, directory);
	}
	/**
	 * Resolves <code>fileName</code> against the configuration directory.
	 *
	 * @param fileName The name of a configuration file.
	 * @param directory The directory of reference.
	 * @return	Returns the absolute path to the specified file.
	 */
	public String resolveFilePath(String fileName, String directory)
	{
		//if (fileName == null)	throw new NullPointerException();
        StringBuffer relPath = new StringBuffer(directory);
        relPath.append(File.separatorChar);
        relPath.append(fileName);
		File f = new File(homeDir, relPath.toString());
		return f.getAbsolutePath();
	}
	
	/**
	 * Returns the container's registry.
	 * 
	 * @return	See above.
	 */
	public Registry getRegistry() { return registry; }
	
	/**
	 * Shut services and exit the JVM process.
	 */
	public void exit()
	{	
		getRegistry().getWatcher().stop();
		System.exit(0);
	}
	
}
