package example.env.ui;

import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.BoxLayout;

import example.util.CommonsUtil;

/**
 * Displays the list of inputs and output files.
 */
class WatcherScreenUI
	extends JFrame
{
	private WatcherScreenController controller;

	/** Starts to watch the repository.*/
	private JButton watchButton;
	
	/** Stop to watch the repository.*/
	private JButton stopButton;
	
	/** Close the application.*/
	private JButton closeButton;

	/**
	 * Attach the listener
	 * 
	 * @param button The component to listen to.
	 * @param cmd The action command
	 */
	private void setlistener(JButton button, int cmd) {
		button.setActionCommand(""+cmd);
		button.addActionListener(controller);
		
	}
	
	/** Initialise the components.*/
	private void initComponents() {
		watchButton = new JButton("Start watching");
		watchButton.setToolTipText("Start watching the directory");
		setlistener(watchButton, WatcherScreenController.WATCH);
		stopButton = new JButton("Stop watching");
		stopButton.setToolTipText("Stop watching the directory");
		stopButton.setEnabled(false);
		setlistener(stopButton, WatcherScreenController.STOP);
		closeButton = new JButton("Close");
		closeButton.setToolTipText("Close and stop watching the directory");
		setlistener(closeButton, WatcherScreenController.CLOSE);
	}

	/** Lay out the controls.*/
	private JPanel buildControlsBar() {
		JPanel bar = new JPanel();
		bar.add(closeButton);
		bar.add(Box.createHorizontalStrut(5));
		bar.add(stopButton);
		bar.add(Box.createHorizontalStrut(5));
		bar.add(watchButton);
		return CommonsUtil.buildComponentPanel(bar, 5, 5, true);
		
	}
	
	/**
	 * Lay out the directory's names
	 * @return
	 */
	private JPanel buildHeader() {
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		JLabel label = new JLabel("Watching: "+controller.getInputDir());
		p.add(label);
		label = new JLabel("Output written in: "+controller.getOutputDir());
		p.add(label);
		return p;
		
	}
	/** Lay out the components.*/
	private void buildUI()
	{
		Container container = getContentPane();
		container.setLayout(new BorderLayout(0, 0));
		container.add(buildHeader(), BorderLayout.NORTH);
		container.add(buildControlsBar(), BorderLayout.SOUTH);
	}

	/**
	 * Creates a new instance.
	 *
	 * @param controller Reference to the controller.
	 */
	WatcherScreenUI(WatcherScreenController controller) {
		this.controller = controller;
		initComponents();
		buildUI();
		setSize(400, 400);
	}
	
	/** Close and dispose.*/
	void close()
	{
		setVisible(false);
		dispose();
	}
	
	/**
	 * Set the enabled flag of the stop/watch controls
	 * @param enable
	 */
	void enableControls(boolean enable) {
		watchButton.setEnabled(enable);
		stopButton.setEnabled(!enable);
	}
}
