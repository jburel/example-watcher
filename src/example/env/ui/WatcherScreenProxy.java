package example.env.ui;

import example.env.config.Registry;

class WatcherScreenProxy
	implements WatcherScreen
{

	
	/** The real subject where the component's functionality sits. */
	private WatcherScreenController servant;
	
	private WatcherScreenUI view;
	
	WatcherScreenProxy(Registry registry) {
		servant = new WatcherScreenController(registry);
		view = new WatcherScreenUI(servant);
		servant.setView(view);
	}
	
	@Override
	public void activate() {
		view.setVisible(true);
		
	}

	@Override
	public void terminate() {
		view.setVisible(false);
		view.dispose();
		
	}

}
