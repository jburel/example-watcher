package example.env.ui;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import example.env.Lookup;
import example.env.config.Registry;

class WatcherScreenController
	implements ActionListener
{

	static final int WATCH = 0;
	
	static final int CLOSE = 1;
	
	static final int STOP = 2;
	
	/** Reference to the context. */
	private Registry registry;
	
	/** Reference to the view. */
	private WatcherScreenUI view;

	/** Stop watching and close the frame*/
	private void onClose() 
	{
		view.close();
		registry.shutDown();
	}
	
	/** Start or stop watching */
	private void onWatch(boolean watch) {
		if (watch) {
			registry.getWatcher().start();
		} else {
			registry.getWatcher().stop();
		}
		view.enableControls(!watch);
	}
	
	/**
	 * Creates a new instance.
	 * @param registry
	 */
	WatcherScreenController(Registry registry) {
		this.registry = registry;
	}
	
	/**
	 * Link view
	 * @param view
	 */
	void setView(WatcherScreenUI view)
	{
		this.view = view;
		view.addWindowListener(new WindowAdapter() {
			
			
			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	/**
	 * Return the directory to watch
	 * @return
	 */
	String getInputDir() {
		return (String) registry.lookup(Lookup.INPUT_DIR);
	}
	
	/**
	 * Return the directory where the output is written
	 * @return
	 */
	String getOutputDir() {
		return (String) registry.lookup(Lookup.OUTPUT_DIR);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		int value = Integer.parseInt(cmd);
		switch (value) {
		case WATCH:
			onWatch(true);
			break;
		case CLOSE:
			onClose();
			break;
		case STOP:
			onWatch(false);
			
		}
	}
}
