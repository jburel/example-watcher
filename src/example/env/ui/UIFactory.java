package example.env.ui;

import example.env.config.Registry;


public class UIFactory {

	/**
	 * Creates the screen used to display results
	 * @param registry Reference to the container.
	 * @return See above.
	 */
	public static final WatcherScreen createScreen(Registry registry) {
		return new WatcherScreenProxy(registry);
	}
}
