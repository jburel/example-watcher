package example.env.ui;


public interface WatcherScreen {
	
	/** 
	 * Container tells to get ready for service.
	 */
    public void activate();
    
	/** Container tells to release acquired resources and stop service. */
    public void terminate();
    
}
