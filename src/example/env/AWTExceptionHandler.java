package example.env;


public final class AWTExceptionHandler {

	/**
	 * Sets up exception handling relay.
	 * Only called by {@link AbnormalExitHandler}. 
	 */
	static void register()
	{
		System.setProperty("sun.awt.exception.handler", "brady.AWTExceptionHandler");
	}
	
	/**
	 * Creates a new instance.
	 */
	public AWTExceptionHandler()
	{
		//This constructor is required by the reflection code in the
		//EventDispatchThread.handleException(Throwable) method.
	}
	
	/**
	 * Forwards any uncaught exception in the AWT event-dispatch thread to
	 * the {@link AbnormalExitHandler}.
	 * 
	 * @param t	The exception that was thrown.
	 */
	public void handle(Throwable t)
	{
		//This is the hook method called by the code in the
		//EventDispatchThread.handleException(Throwable) method.
		
		AbnormalExitHandler.terminate(t);
	}

}
