package example.env;

public class Lookup {
	
	/** Field to indicating if the name of the input directory. */
    public static final String INPUT_DIR = "inputPath";
    
    /** Field to indicating if the name of the output directory. */
    public static final String OUTPUT_DIR = "outputPath";
    
    /** Field to indicating if the length of time to watch. */
    public static final String TIME = "duration";

    /** Field to indicating if the data of reference to write data. */
    public static final String REFERENCE_DATA = "referenceData";
    
    public static final String FACTORS = "factors";    
}
