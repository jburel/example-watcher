package example.env.svc;


import java.io.File;
import java.util.List;

import example.env.Container;
import example.env.Lookup;
import example.env.config.Registry;

public class WatcherFactory {

	/*
	 * Creates a new instance.
	 */
	public static Watcher makeNew(Container c)
	{
		Registry reg = c.getRegistry();
		String input = (String) reg.lookup(Lookup.INPUT_DIR);
		String output = (String) reg.lookup(Lookup.OUTPUT_DIR);
		List<Object> factors = (List) reg.lookup(Lookup.FACTORS);
		File inputDir = new File(input);
		if (!inputDir.exists()) {
			inputDir.mkdir();
		}
		File outputDir = new File(output);
		if (!outputDir.exists()) {
			outputDir.mkdir();
		}
		
		return new WatcherImpl(inputDir, outputDir, factors);
	}
}
