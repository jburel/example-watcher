package example.env.svc;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import example.env.model.InputParser;
import example.env.model.OutputWriter;
import example.util.CommonsUtil;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

class WatcherImpl 
	implements Watcher, Runnable
{

	/** The directory to watch.*/
	private File inputDir;
	
	/** The directory to watch the output.*/
	private File outputDir;
	
	private ExecutorService mExecutor;
	
	private Future<?> mWatcherTask;
	
	private Map<String, String> parsedFiles;
	
	private List<Object> factors;
	
	private boolean validFile(String name) {
		if (!CommonsUtil.isExtension(name, "xml")) {
			return false;
		}
		return true;
	}
	
	/**
	 * The input file has been modified, update the output.
	 * @param context
	 */
	private void updateOutput(Path context) {
		deleteOutput(context);
		parseFile(context);
	}
	
	/**
	 * The input file has been deleted, delete the output.
	 * @param context
	 */
    private void deleteOutput(Path context) {
    	String name = context.toString();
    	//file has not been parsed
    	String output = parsedFiles.get(name);
    	if (CommonsUtil.isBlank(output)) {
    		return;
    	}
    	//CheckoutDir and delete file
    	File f = new File(outputDir, output);
    	f.delete();
    	parsedFiles.remove(name);
	}

    /**
     * Generate the output.
     * @param context
     */
	private void parseFile(Path context)
	{
		String name = context.toString();
		String output = parsedFiles.get(name);
		if (CommonsUtil.isBlank(output) && validFile(name)) {
			InputParser parser = new InputParser();
			OutputWriter writer = new OutputWriter(outputDir, factors);
			File f = new File(inputDir, name);
			String n = CommonsUtil.getFileNameNoExtension(name);
			String outputName = n+"_output.xml";
			output = writer.writeOutput(outputName, parser.parse(f.getAbsolutePath()));
			if (!CommonsUtil.isBlank(output)) {
				parsedFiles.put(name, output);
			}
		}
	}

	
	/**
	 * Creates a new instance.
	 * 
	 * @param inputDir The directory to watch.
	 * @param outputDir Folder where to write the output.
	 * @param factors The common factor
	 */
	WatcherImpl(File inputDir, File outputDir, List<Object> factors) {
		parsedFiles = new HashMap<String, String>();
		this.inputDir = inputDir;
		this.outputDir = outputDir;
		this.factors = factors;
	}

	@Override
	public void start() {
		mExecutor = Executors.newSingleThreadExecutor();
		mWatcherTask = mExecutor.submit(this);
		
	}

	@Override
	public void stop() {
		if (mWatcherTask != null) {
			mWatcherTask.cancel(true);
	        mWatcherTask = null;
		}
        if (mExecutor != null) {
        	mExecutor.shutdown();
            mExecutor = null;
        }
	}

	@Override
	public void run() {
		try (WatchService watchService = FileSystems.getDefault().newWatchService()) {

			Path path = Paths.get(inputDir.getAbsolutePath());

		      path.register(watchService, ENTRY_CREATE, ENTRY_MODIFY);

		      while (true) {
		    	  if (Thread.interrupted()) {
		                //log error
		                break;
		          }
		          WatchKey key = watchService.take();

		          for (WatchEvent<?> watchEvent : key.pollEvents()) {

		            Kind<?> kind = watchEvent.kind();
		            Path context = (Path) watchEvent.context();
		            
		            System.out.printf("%s:%s\n", context, kind);
		            if (kind.equals(ENTRY_CREATE)) {
		            	parseFile(context);
		            } else if (kind.equals(ENTRY_MODIFY)) {
		            	updateOutput(context);
		            }
		          }

		          boolean valid = key.reset();
		          if (!valid) {
		            break;
		          }
		      }
		      
		 } catch (IOException e) {
			 //Add logger
		 } catch (InterruptedException ie) {
			 
		 }
	}

}
