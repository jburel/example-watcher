package example.env.svc;


public interface Watcher
{

	/** Start watching a directory.*/
	public void start();
	
	/** Stop watching a directory.*/
	public void stop();
	
}
