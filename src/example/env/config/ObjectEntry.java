package example.env.config;

import org.w3c.dom.Node;

class ObjectEntry 
	extends Entry
{

	/** The lookup name. Overrides the same field in {@link Entry}. */
	//NB: we do this b/c Entry.name has to be kept private to avoid subclasses
	//screwing around and modifying mappings.
	private String		name;
	
	/** The value. */
	private Object		value;
	
	/**
	 * Creates a new entry to be added to the registry.
	 * 
	 * @param name	The lookup name given to this entry.
	 */
	ObjectEntry(String name)
	{
		this.name = name;
	}

	/** 
	 * Returns this entry's value.
	 * @see Entry#getValue()
	 */
	Object getValue() { return value; }

	/**
	 * Does nothing as this entry is created by the 
	 * {@link Registry#bind(String, Object) bind} method.
	 * @see Entry#setContent(org.w3c.dom.Node)
	 */
	protected void setContent(Node node)
	{
		//Do nothing. This is an in-memory entry.	
	}

	/** 
	 * Stores <code>content</code> as this entry's value.
	 * 
	 * @param content	The entry's value.
	 */
	void setContent(Object content) { value = content; }
	
	/** 
     * Overrides the superclass method to return the correct value. 
     * @see Entry#getName()
     */
     @Override
	String getName() { return name; }
    
}
