package example.env.config;

import example.env.Container;
import example.env.svc.Watcher;

/**
 * A collection of factory methods to create a {@link Registry} and helper
 * methods to manipulate one.
 * <p>Helper methods are needed so that we may link container's services 
 * to a registry without having to know about the actual {@link Registry}'s
 * implementation class &#151; this is required by some classes that perform 
 * initialization tasks.</p>
 *
 */
public class RegistryFactory {

	/**
	 * Creates a new empty {@link Registry}.
	 * 
	 * @return	See above.
	 */
	public static Registry makeNew(Container c) { return new RegistryImpl(c); }
	
	/**
	 * Fills up the specified {@link Registry} with the entries
	 * in the specified configuration file.
	 * 
	 * @param file	Path to a configuration file.
	 * @param reg	The {@link Registry} to fill.
	 * @throws ConfigException	If an error occurs while accessing the file
	 * 							or the file contents are not valid.
	 */
	public static void fillFromFile(String file, Registry reg)
		throws ConfigException
	{
		Parser p = new Parser(file, (RegistryImpl) reg);
		p.parse();
	}

	/**
	 * Reads the reference data file
	 * @param file
	 * @param reg
	 * @throws ConfigException
	 */
	public static void fillReferenceData(String file, Registry reg)
			throws ConfigException
	{
		Parser p = new Parser(file, (RegistryImpl) reg);
		p.parseReference();
	}
	
	/**
	 * Link the directory watcher
	 * @param Watcher
	 * @param reg
	 */
	public static void linkWatcher(Watcher Watcher, Registry reg)
	{
		((RegistryImpl) reg).setWatcher(Watcher);
	}
    
}
