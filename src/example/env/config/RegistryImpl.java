package example.env.config;

import java.util.HashMap;
import java.util.Map;

import example.env.Container;
import example.env.svc.Watcher;

class RegistryImpl 
	implements Registry
{

	private final Container container;
	
	/** The name-value map. */
    private final Map<String, Object> entriesMap = new HashMap<String, Object>();
    
    /** Reference to the directory watcher */
    private Watcher watcher;

    RegistryImpl(Container c)
    {
    	this.container = c;
    }
	@Override
	public void bind(String name, Object value) {
		if (name != null) {
			ObjectEntry entry = new ObjectEntry(name);
			entry.setContent(value);
			entriesMap.put(name, entry);
		}
	}

	@Override
	public Object lookup(String name) {
		Entry entry = (Entry) entriesMap.get(name);
        if (entry != null) {
        	return entry.getValue();
        }
        return null;
	}
	
   /** 
	* Adds the specified {@link Entry} to the map.
	*
	* @param e 	A new {@link Entry} created from an entry tag in the
	* 			configuration file.
	*/
	void addEntry(Entry e) { entriesMap.put(e.getName(), e); }

	/**
	 * Set the directory watcher
	 * @param watcher
	 */
	void setWatcher(Watcher watcher) {
		this.watcher = watcher;
	}

	@Override
	public Watcher getWatcher() {
		return watcher;
	}
	
	/** Shut down.*/
	public void shutDown()
	{
		container.exit();
	}
}
