package example.env.config;

import org.w3c.dom.DOMException;
import org.w3c.dom.Node;

/**
 * Handles an <i>entry</i> of type <i>string</i>.
 * The tag's value is stored into a {@link String} object which is then
 * returned by the {@link #getValue() getValue} method.
 *
 */
class StringEntry
	extends Entry
{

	/** The entry value. */
    private String value;
    
    
	/** Creates a new instance. */
    public StringEntry() {}
    
    /** 
     * Implemented as specified by {@link Entry}. 
     * @see Entry#setContent(Node)
     * @throws ConfigException If the configuration entry couldn't be handled.
     */ 
    protected void setContent(Node node)
    	throws ConfigException
    { 
        try {
            value = node.getFirstChild().getNodeValue();
		} catch (DOMException dex) { 
			rethrow("Can't parse string entry, name: "+getName()+".", dex);
		}
    }
    
	/**
	 * Returns a {@link String} object which represents the tag's content.
	 * 
	 * @return	See above.
	 */    
    Object getValue() { return value; }
    
}
