package example.env.config;


/**
 * Reports an error occurred while configuring a registry.
 *
 */
public class ConfigException
	extends Exception
{

	/**
	 * Constructs a new exception with the specified detail message.
	 * 
	 * @param message	Short explanation of the problem.
	 */
	public ConfigException(String message) { super(message); }

	/**
	 * Constructs a new exception with the specified detail message and cause.
	 * 
	 * @param message	Short explanation of the problem.
	 * @param cause		The exception that caused this one to be risen.
	 */
	public ConfigException(String message, Throwable cause) 
	{
		super(message, cause);
	}

}
