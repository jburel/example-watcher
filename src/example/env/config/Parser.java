package example.env.config;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import javax.xml.parsers.DocumentBuilder; 
import javax.xml.parsers.DocumentBuilderFactory;  

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import example.env.Lookup;
import example.env.model.Factor;

/**
 * Fills up a registry with the entries in a configuration file.
 * Parses a configuration file, extracts its entries (only <code>entry</code>
 * and <code>structuredEntry</code> tags are taken into account), obtains an
 * {@link Entry} object to represent each of those entries and adds these
 * objects to a given {@link RegistryImpl} object. 
 *
 */
class Parser {

	/** The tags that we handle. */
	static private String[] tagsEntry = {Entry.ENTRY};
	
	/** The tags that we handle. */
	static private String[] tagsFactor = {Factor.EMISSIONS, Factor.VALUE};
	
	/** 
	 * Tells whether or not we're validating the configuration file against
	 * a schema.
	 */  
	private boolean validating;
	
	/** The configuration file. */
    private Document document;
    
    /** Points to the configuration file. */
    private String configFile;
    
    /** Collects all tags that we have to handle from the configuration file. */
    private List<Node> entriesTags;
    
    /** The registry that we have to fill up. */
    private RegistryImpl registry;
    
    /**
     * Wraps the original exception into a {@link ConfigException}, which is
     * then re-thrown with an error message.
     * 
     * @param e The original exception.
     * @throws ConfigException  Wraps the original exception and contains an
     *                          error message.
     */
    private void rethrow(Exception e)
        throws ConfigException
    {
        StringBuffer msg = new StringBuffer(
                            "An error occurred while attempting to process ");
        msg.append(configFile);
        msg.append(".");
        String explanation = e.getMessage();
        if (explanation != null && explanation.length() != 0) {
            msg.append(" (");
            msg.append(explanation);
            msg.append(")");    
        }
        throw new ConfigException(msg.toString(), e); 
    }
    
    /** 
     * Retrieves the content of the tags that we handle.
     * Stores their DOM representation (DOM node) into a list.
     */
    private void readConfigEntries()
    {
        NodeList list;
        Node n;
        for (int k = 0; k < tagsEntry.length; ++k) {
            list = document.getElementsByTagName(tagsEntry[k]);
            for (int i = 0; i < list.getLength(); ++i) {
                n = list.item(i);
                if (n.hasChildNodes()) {
                	entriesTags.add(n);
                }
            }
        }
    }

    /**
     * Reads the factors.
     * @return
     */
    private List<Factor> readFactors()
    {
    	NodeList list;
        Node n;
        List<Factor> factors = new ArrayList<Factor>();
        for (int k = 0; k < tagsFactor.length; ++k) {
            list = document.getElementsByTagName(tagsFactor[k]);
            for (int i = 0; i < list.getLength(); ++i) {
                n = list.item(i);
                if (n.hasChildNodes()) {
                	Factor f = new Factor(n.getNodeName());
                	NodeList nodes = n.getChildNodes();
            		for (int j = 0; j < nodes.getLength(); j++) {
            			Node nn = nodes.item(j);
            			if (nn.hasChildNodes()) {
            				f.setContent(nn.getNodeName(), nn.getFirstChild().getNodeValue());
            			}
            		}
                	factors.add(f);
                }
            }
        }
        return factors;   
    }
    
	/** 
	 * Creates a new instance to fill up the specified registry with the
	 * entries from the specified configuration file.
	 * The configuration file is not validated against a schema.
	 *
	 * @param configFile	Path to the configuration file.
	 * @param registry		The registry to fill up.         
	 */
    Parser(String configFile, RegistryImpl registry)
    { 
		validating = false;
        this.configFile = configFile;
        this.registry = registry;
		entriesTags = new ArrayList<Node>();
    }
    
	/** 
	 * Parses the configuration file, extracts its entries (only
	 * <code>entry</code> and <code>structuredEntry</code> tags are taken into
	 * account), obtains an {@link Entry} object to represent each of those
	 * entries and adds these objects to the given {@link RegistryImpl} object.
	 * 
	 * @throws ConfigException	If an error occurs and the registry can't be
	 * 							filled up.
	 */
    void parse()
    	throws ConfigException
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try (FileInputStream stream = new FileInputStream(configFile)) {
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(stream);
            if (validating) {
                factory.setValidating(true);   
                factory.setNamespaceAware(true);
            }
            readConfigEntries();
            Iterator<Node> i = entriesTags.iterator();
			Node node;
			Entry entry;
            while (i.hasNext()) {
               node = (Node) i.next();
               entry = Entry.createEntryFor(node);
               registry.addEntry(entry);
            }
        } catch (ConfigException ce) {
        	throw ce;
        } catch (Exception e) { 
        	rethrow(e);
        }
    }
   
    void parseReference()
    	throws ConfigException
    {
    	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    	try (FileInputStream stream = new FileInputStream(configFile)) {
    		 DocumentBuilder builder = factory.newDocumentBuilder();
             document = builder.parse(stream);
             List<Factor> factors = readFactors();
             registry.bind(Lookup.FACTORS, factors);
    	} catch (Exception e) {
    		e.printStackTrace();
        	rethrow(e);
        }
    }
}
