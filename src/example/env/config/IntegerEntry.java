package example.env.config;

import org.w3c.dom.DOMException;
import org.w3c.dom.Node;

/**
 * Handles an <i>entry</i> of type <i>integer</i>.
 * The tag's value is stored into a {@link Integer} object which is then
 * returned by the {@link #getValue() getValue} method.
 */
class IntegerEntry
	extends Entry
{
    
	/** The entry value. */
    private Integer value;
    
    
	/** Creates a new instance. */
    public IntegerEntry() {}
    
    /** 
     * Implemented as specified by {@link Entry}. 
     * @see Entry#setContent(Node)
     * @throws ConfigException If the configuration entry couldn't be handled.
     */  
    protected void setContent(Node node)
		throws ConfigException
    { 
		String cfgVal = null;
		try {
			cfgVal = node.getFirstChild().getNodeValue();
            value = Integer.valueOf(cfgVal);
		} catch (DOMException dex) { 
			rethrow("Can't parse integer entry, name: "+getName()+".", dex);
		} catch (NumberFormatException nfe) {
			rethrow(cfgVal+" is not a valid integer, entry name: "+
					getName()+".", nfe);
		}
    }
    
	/**
	 * Returns a {@link Integer} object which represents the tag's content.
	 * The double value wrapped by the returned object will be parsed as
	 * specified by the {@link Integer} class. 
	 * 
	 * @return	See above.
	 */  
    Object getValue() { return value; }
}
