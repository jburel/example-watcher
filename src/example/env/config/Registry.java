package example.env.config;

import example.env.svc.Watcher;

public interface Registry {

	/** 
	 * Maps <code>name</code> onto <code>value</code>.
	 * The object can then be retrieved by passing <code>name</code> to the
	 * {@link #lookup(String) lookup} method.  
	 * 
	 * @param name	This name of the entry. If <code>null</code>, this method
	 * 				does nothing.		
	 * @param value	The object to map onto <code>name</code>.
	 */
	public void bind(String name, Object value);
	
	/**
	 * Retrieves the object keyed by <code>name</code> from the registry.
	 * The <code>name</code> parameter is either the value of the <i>name</i>
	 * attribute of an entry tag in the configuration file (in this case the
	 * object that represents the configuration entry will be returned) or the
	 * name to which the object was bound &#151; by means of the
	 * {@link #bind(String, Object) bind} method (in this case the object that
	 * was originally passed to {@link #bind(String, Object) bind()} will be 
	 * returned). 
	 * 
	 * @param name	The name which an object within this registry is
	 * 				mapped onto.
	 * @return The object mapped to <code>name</code> or <code>null</code> if
	 * 			no such a mapping exists.
	 */  
	public Object lookup(String name);
	
	/**
	 * Return the directory watcher
	 * @return
	 */
	public Watcher getWatcher();
	
	public void shutDown();
	
}
