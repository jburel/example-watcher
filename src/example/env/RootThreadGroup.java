package example.env;

final class RootThreadGroup
	extends ThreadGroup
{

	/** The name of the thread group. */
	private static final String	NAME = "TestApp";
	

	/** Creates a new instance. */
	RootThreadGroup()
	{
		super(NAME);
	}
	
	/**
	 * Overrides the parent's method to activate the abnormal exit procedure
	 * in the case of an uncaught exception.
     * @see ThreadGroup#uncaughtException(Thread, Throwable)
	 */
	public void uncaughtException(Thread t, Throwable e)
	{
		AbnormalExitHandler.terminate(e);	
	}

}
