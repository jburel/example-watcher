package example.env.model;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class OutputWriter {
	
	private File outputDir;
	
	private Factor valueFactor;
	
	private Factor emissionsFactor;
	
	private Document doc;
	
	/**
	 * Extract the factor
	 * @param factors
	 */
	private void initFactors(List<Object> factors) {
		Iterator<Object> i = factors.iterator();
		while (i.hasNext()) {
			Factor f = (Factor) i.next();
			if (f.getName().equals(Factor.VALUE)) {
				valueFactor = f;
			} else {
				emissionsFactor = f;
			}
		}
	}
	
	/**
	 * Return the generation value.
	 * @param g The generator to handle
	 * @return
	 */
	private Element getTotalGenerationValue(Generator g) {
		Element node = doc.createElement("Generator");
		List<DailyGeneration> values = g.getGeneration();
		
		Iterator<DailyGeneration> i = values.iterator();
		Double factor = valueFactor.getFactor(g.getFactorIndex());
		double total = 0;
		double f = factor.doubleValue();
		while (i.hasNext()) {
			DailyGeneration dg = i.next();
			total += dg.getDailyValue(f);
		}
		Element n = doc.createElement("Name");
		n.appendChild(doc.createTextNode(g.getName()));
		node.appendChild(n);
		n = doc.createElement("Total");
		n.appendChild(doc.createTextNode(""+total));
		node.appendChild(n);
		return node;
		
	}
	
	/**
	 * Create heat rate output
	 * 
	 * @param g The generator to handle
	 * @param node The parent node.
	 */
	private void getHeatRate(CoalGenerator g, Element node) {
		Element n = doc.createElement("Name");
		n.appendChild(doc.createTextNode(g.getName()));
		node.appendChild(n);
		n = doc.createElement("HeatRate");
		n.appendChild(doc.createTextNode(""+g.getHeatRate()));
		node.appendChild(n);
	}
	
	/**
	 * Create the max report.
	 * @param parent The parent node
	 * @param values The values to handle
	 */
	private void createMaxReport(Element parent, Collection<DayResult> values) {
		Iterator<DayResult> i = values.iterator();
		while (i.hasNext()) {
			Element node = doc.createElement("Day");
			parent.appendChild(node);
			DayResult r = i.next();
			Element n = doc.createElement("Name");
			n.appendChild(doc.createTextNode(r.getName()));
			node.appendChild(n);
			n = doc.createElement("Date");
			n.appendChild(doc.createTextNode(r.getDate()));
			node.appendChild(n);
			n = doc.createElement("Emission");
			n.appendChild(doc.createTextNode(r.getEmission()));
			node.appendChild(n);
		}
	}
	
	/**
	 * Create a new instance
	 * 
	 * @param outputDir Directory where to write the output.
	 * @param factors The reference factor
	 */
	public OutputWriter(File outputDir, List<Object> factors)
	{
		this.outputDir = outputDir;
		initFactors(factors);
	}

	/**
	 * Write the report. Return the absolute path to the file
	 * 
	 * @param name The output name
	 * @param generators The data to handle
	 * @return
	 */
	public String writeOutput(String name, List<Generator> generators) {
		System.err.println("generators:"+generators);
		if (generators == null || generators.size() == 0) {
			return null;
		}
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	        doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("GenerationOutput");
			doc.appendChild(rootElement);
			Element totals = doc.createElement("Totals");
			rootElement.appendChild(totals);
			
			Element rates = doc.createElement("ActualHeatRates");
			rootElement.appendChild(rates);
			
			Element max = doc.createElement("MaxEmissionGenerators");
			rootElement.appendChild(max);
			

	        Iterator<Generator> i = generators.iterator();
	        Map<String, DayResult> results = new HashMap<String, DayResult>();
			while (i.hasNext()) {
				Generator g = i.next();
				Element node = getTotalGenerationValue(g);
				totals.appendChild(node);
				if (!(g instanceof WindGenerator)) {
					GasGenerator gg = (GasGenerator) g;
					double factor = emissionsFactor.getFactor(g.getFactorIndex());
					Map<String, Double> values = gg.getDailyEmission(factor);
					Iterator<String> j = values.keySet().iterator();
					while (j.hasNext()) {
						String key = j.next();
						DayResult r = results.get(key);
						if (r == null) {
							r = new DayResult(key);
							results.put(key, r);
						}
						r.setEmissionValue(values.get(key), g.getName());
					}
				}
				if (g instanceof CoalGenerator) { 
					getHeatRate((CoalGenerator) g, rates);
				}
			}
			//Create the tags
			createMaxReport(max, results.values());
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			File f = new File(outputDir, name);
			StreamResult result = new StreamResult(f);

			// Output to console for testing
			transformer.transform(source, result);
			System.err.println("OutputWritten");
			return f.getAbsolutePath();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return null;
	}
}
