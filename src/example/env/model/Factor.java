package example.env.model;

public class Factor {

	public static final String VALUE = "ValueFactor";
	
	public static final String EMISSIONS = "EmissionsFactor";

	public static final int OFFSHORE = 0;
	
	public static final int ONSHORE = 1;
	
	public static final int GAS = 2;
	
	public static final int COAL = 3;
	
	private static final String HIGH = "High";
	
	private static final String LOW = "Low";
	
	private static final String MEDIUM = "Medium";
	
	private Double high;
	private Double medium;
	private Double low;
	
	private String name;
	
	/**
	 * Return the value factor according to the generator
	 * @param type
	 * @return
	 */
	private Double getValueFactor(int type) {
		switch (type) {
		case OFFSHORE:
			return low;
		case ONSHORE:
			return high;
		case GAS:
		case COAL:
			return medium;
		}
		return null;
	}
	
	/**
	 * Return the emissions factor according to the generator
	 * @param type
	 * @return
	 */
	private Double getEmissionsFactor(int type) {
		switch (type) {
		case OFFSHORE:
		case ONSHORE:
			return null;
		case GAS:
			return medium;
		case COAL:
			return high;
		}
		return null;
	}

	/**
	 * Parse the string
	 * @param value
	 * @return
	 */
	private Double parseValue(String value)
	{
		try {
			return Double.valueOf(value);
		} catch (Exception e) {}
		return null;
	}
	
	/**
	 * Create a new instance
	 * @param name
	 */
	public Factor(String name) {
		this.name = name;
	}
	
	/**
	 * Populate the various fields
	 * 
	 * @param name
	 * @param value
	 */
	public void setContent(String name, String value) {
		if (name.equals(HIGH)) {
			high = parseValue(value);
		} else if (name.equals(MEDIUM)) {
			medium = parseValue(value);
		} else if (name.equals(LOW)) {
			low = parseValue(value);
		}
	}
	
	/**
	 * Return the factor matching the type
	 * @param type
	 * @return
	 */
	Double getFactor(int type) {
		if (VALUE.equals(name)) {
			return getValueFactor(type);
		}
		return getEmissionsFactor(type);
	}
	
	/**
	 * Return the factor's name
	 * @return
	 */
	String getName() { return name; }
	
	
}
