package example.env.model;

class DailyGeneration {

	static final String DAY = "Day";
	
	static final String DATE = "Date";
	
	static final String ENERGY = "Energy";
	
	static final String PRICE = "Price";
			
	private String date;
	
	private Double energy;
	
	private Double price;

	DailyGeneration() {}
	
	void setContent(String name, String value) {
		if (DATE.equals(name)) {
			date = value;
		} else if (ENERGY.equals(name)) {
			try {
				energy = Double.parseDouble(value);
			} catch (Exception e) {}
		} else if (PRICE.equals(name)) {
			try {
				price = Double.parseDouble(value);
			} catch (Exception e) {}
		}
	}
	
	String getDate() { return date; }
	
	double getEnergy() {
		if (energy == null) {
			return 0;
		}
		return energy.doubleValue();
	}
	
	double getDailyValue(double factor) {
		if (energy == null || price == null) {
			return 0;
		}
		return energy.doubleValue()*price.doubleValue()*factor;
	}
	
}
