package example.env.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Generator {

	static final String WIND = "WindGenerator";
	
	static final String GAS = "GasGenerator";
	
	static final String COAL = "CoalGenerator";
	
	static final String NAME = "Name";
	
	static final String GENERATION = "Generation";
	
	static final String LOCATION = "Location";
	
	static final String RATING = "EmissionsRating";
	
	static final String INPUT = "TotalHeatInput";
	
	static final String NET_GENERATION = "ActualNetGeneration";
	
	/** The name of the generator*/
	protected String name;
	
	/** The collection of daily output*/
	private List<DailyGeneration> generation;
	
	/** Create a new instance*/
	Generator() {
		generation = new ArrayList<DailyGeneration>();
	}
	
	/**
	 * Set the content. To be overridden
	 * @param key
	 * @param value
	 */
	protected void setContent(String key, String value) {};
	
	/** 
	 * Return the generator factor. To be overridden
	 * @return
	 */
	protected int getFactorIndex() { return -1; };
	
	/**
	 * Add a new daily output
	 * @param value
	 */
	void addGeneration(DailyGeneration value) {
		generation.add(value);
	}
	
	/**
	 * Set the name of the generator
	 * @param name
	 */
	void setName(String name) { this.name = name; }
	
	/**
	 * Return the name of the generator
	 * @return
	 */
	String getName() { return name; }
	
	/**
	 * Return the collection of daily output
	 * @return
	 */
	List<DailyGeneration> getGeneration() {
		return Collections.unmodifiableList(generation);
	}

}
