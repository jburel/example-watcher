package example.env.model;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import example.util.CommonsUtil;

public class InputParser {

	
	/** The tags that we handle. */
    private static String[] tagsGenerator = {Generator.WIND,
    		Generator.GAS, Generator.COAL};
    
	private Document document;
	
	/**
	 * Create generation.
	 * 
	 * @param node The node to handle.
	 * @return See above.
	 */
	private void createGeneration(Generator g, Node node)
	{
		NodeList nodes = node.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node n = nodes.item(i);
			if (n.hasChildNodes()) {
				String name = n.getNodeName();
				if (DailyGeneration.DAY.equals(name)) {
					DailyGeneration dg = new DailyGeneration();
					g.addGeneration(dg);
					NodeList children = n.getChildNodes();
					for (int j = 0; j < children.getLength(); j++) {
						Node nn = children.item(j);
						if (nn.hasChildNodes()) {
							dg.setContent(nn.getNodeName(), nn.getFirstChild().getNodeValue());
						}
					}
				}
				
			}
		}
	}
	
	/**
	 * Create a generator
	 * @param node The node to handle.
	 * @return See above
	 */
	private Generator createGenerator(Node node) {
		
		String name = node.getNodeName();
		Generator generator = null;
		if (Generator.WIND.equals(name)) {
			generator = new WindGenerator();
		} else if (Generator.GAS.equals(name)) {
			generator = new GasGenerator();
		} else if (Generator.COAL.equals(name)) {
			generator = new CoalGenerator();
		}
		NodeList nodes = node.getChildNodes();
		
		for (int i = 0; i < nodes.getLength(); i++) {
			Node n = nodes.item(i);
			if (n.getNodeType() == Node.ELEMENT_NODE) {
				name = n.getNodeName();
				if (Generator.NAME.contentEquals(name)) {
					generator.setName(n.getFirstChild().getNodeValue());
				} else if (Generator.GENERATION.contentEquals(name)) {
					createGeneration(generator, n);
				} else {
					generator.setContent(name, n.getFirstChild().getNodeValue());
				}
			}
		}
		return generator;
	}
	
	/**
	 * Read the generator
	 * 
	 * @return
	 */
	private List<Generator> readGeneratorEntries() {
		NodeList list;
        Node n;
        List<Generator> generators = new ArrayList<Generator>();
        for (int k = 0; k < tagsGenerator.length; ++k) {
            list = document.getElementsByTagName(tagsGenerator[k]);
            for (int i = 0; i < list.getLength(); ++i) {
                n = list.item(i);
                generators.add(createGenerator(n));
            }
        }
        return generators;
	}
	
	/**
	 * Creates a new instance.
	 * 
	 * @param outputDir
	 */
	public InputParser() {}
	
	/**
	 * Parses the file.
	 * @param file
	 */
	public List<Generator> parse(String file)
	{
		if (!CommonsUtil.isExtension(file, "xml")) {
			return null;
		}
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try (FileInputStream stream = new FileInputStream(file)) {
			DocumentBuilder builder = factory.newDocumentBuilder();
			document = builder.parse(stream);
			return readGeneratorEntries();		
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

}
