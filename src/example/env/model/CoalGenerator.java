package example.env.model;

class CoalGenerator
	extends GasGenerator
{

	private Double totalHeatInput;
	
	private Double actualNetGeneration;
	
	CoalGenerator() {
	}
	
	protected int getFactorIndex() { return Factor.COAL; }
	
	protected void setContent(String key, String value) {
		super.setContent(key, value);
		if (INPUT.equals(key)) {
			try {
				totalHeatInput = Double.parseDouble(value);
			} catch (Exception e) {}
		} else if (NET_GENERATION.equals(key)) {
			try {
				actualNetGeneration  = Double.parseDouble(value);
			} catch (Exception e) {}
		}
	}

	/**
	 * Return the heatrate
	 * @return
	 */
	double getHeatRate() {
		if (actualNetGeneration == null || totalHeatInput == null) {
			return 0;
		}
		double d = actualNetGeneration.doubleValue();
		if (d == 0) {
			return 0;
		}
		return totalHeatInput.doubleValue()/d;
	}
}
