package example.env.model;

class DayResult {
	
	
	private String name;
	
	private String date;
	
	private double emission;
	
	DayResult(String date) {
		this.date = date;
		emission = Double.MIN_VALUE;
	}

	void setEmissionValue(double value, String name) {
		if (value >  emission) {
			emission = value;
			this.name = name;
		}
	}
	
	String getEmission() {
		return ""+emission;
	}
	String getDate() {
		return date;
	}
	
	String getName() {
		return name;
	}
}
