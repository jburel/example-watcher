package example.env.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

class GasGenerator 
	extends Generator
{

	protected Double emissionsRating;
	
	GasGenerator() {
	}
	
	protected int getFactorIndex() { return Factor.GAS; }
	
	protected void setContent(String key, String value) {
		if (RATING.equals(key)) {
			try {
				emissionsRating = Double.parseDouble(value);
			} catch (Exception e) {}
		}
	}

	/**
	 * Return the daily emission
	 * @param factor
	 * @return
	 */
	protected Map<String, Double> getDailyEmission(double factor) {
		Map<String, Double> map = new HashMap<String, Double>();
		List<DailyGeneration> values = getGeneration();
		Iterator<DailyGeneration> i = values.iterator();
		double d = 0;
		if (emissionsRating != null) {
			d = emissionsRating.doubleValue();
		}
		while (i.hasNext()) {
			DailyGeneration dg = i.next();
			map.put(dg.getDate(), dg.getEnergy()*d*factor);
		}
		return map;
	}
}
