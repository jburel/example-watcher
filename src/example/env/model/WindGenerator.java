package example.env.model;

class WindGenerator 
	extends Generator
{

	private String location;
	
	WindGenerator() {}
	
	protected void setContent(String key, String value) {
		if (LOCATION.equals(key)) {
			location = value;
		}
	}
	
	String getLocation() { return location; }
	
	protected int getFactorIndex() { 
		String value = location.toLowerCase();
		if (value.equals("offshore")) return Factor.OFFSHORE;
		return Factor.ONSHORE;
	}
}
