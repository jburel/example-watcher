package example.env;

class AbnormalExitHandler {
	/** The sole instance. */
	private static AbnormalExitHandler	singleton;
	
	
	/**
	 * Creates the singleton and sets up AWT exception relay.
	 * This must be called by the container just once and before initialization
	 * takes place.
	 */
	static void configure()
	{
		//Don't need check for singleton existence, 
		//the container will call this only once.
		singleton = new AbnormalExitHandler();
		AWTExceptionHandler.register();	
	}
	
	/**
	 * Notifies the user, logs the exception, and quits the application.
	 * Called by the container if an unhandled exception is detected in the
	 * main thread or by the {@link AbnormalExitHandler} if an exception is
	 * detected in the AWT event-dispatch thread.
	 * 
	 * @param t		The exception that went unhandled.
	 */
	static void terminate(Throwable t)
	{
		singleton.doTermination(t);
		//TODO: use another policy for InternalError.  This is thrown in the
		//case of a bug which doesn't compromise normal functioning "too much".
		//So in this case we can just notify the user and log the error. 
	}
	
    /**
     * Indicates whether termination is already in progress.
     * Latches to true after the first call to the 
     * {@link #doTermination(Throwable) doTermination} method.
     */
    private boolean inProgress = false;
    
	/** Only used for the singleton. */
	private AbnormalExitHandler() {}
	
	/**
	 * Actual implementation of the {@link #terminate(Throwable) terminate}
	 * method.
	 * This method is thread-safe.  An exception might go unhandled in more
	 * then one thread (initialization is performed while the splash screen
	 * is showing), so we have to take into account the possibility that
	 * multiple threads call this method concurrently.
	 * 
	 * @param t		The exception that went unhandled.
	 */
	private synchronized void doTermination(Throwable t)
	{
        //We need to make sure calls to this method are serialized.  
        //The synchronized keywork ensures that only one thread at
        //a time can proceed.  However, the same thread is allowed
        //to call this method again -- locks are re-entrant, so in
        //the case of recursion the original caller would enter again.
        //So just exit to avoid possible infinite loops if another
        //exception is thrown by the user notifier dialog (see below).
        if (inProgress) System.exit(1);
        
        //First call, set termination flag in case this method is 
        //called again.
        inProgress = true;
				

		System.exit(1);
	}

}
