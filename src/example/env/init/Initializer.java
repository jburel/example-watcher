package example.env.init;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import example.env.Container;

public class Initializer {

	/**
     * Lists the tasks that will make up the initialization sequence.
     * Note that this list can be accessed by subclasses.  This is key in
     * testing environments because initialization tasks can be replaced
     * with tasks that provide service stubs to remove problematic 
     * dependencies on external resources.
     */
    protected static final List<Class<?>>     initList = new ArrayList<Class<?>>();
    
    static {
        initList.add(ConfigInit.class);
        initList.add(WatcherInit.class);
        initList.add(WatcherScreenInit.class);
    }
    
    /**
     * Instantiates the specified initialization task and links it to the
     * passed {@link Container} and {@link Initializer}.
     * 
     * @param taskType  The class of the task.  Mustn't be <code>null</code>.
     * @param c         Reference to the {@link Container}.
     *                  Mustn't be <code>null</code>.
     * @param i         Reference to the {@link Initializer}.
     *                  Mustn't be <code>null</code>.
     * @return An instance of the requested task type.
     * @throws StartupException If the task couldn't be created.
     */
    private static InitTask createInitTask(Object taskType, 
                                                    Container c, Initializer i) 
        throws Exception
    {
        //Sanity checks.
        if (taskType == null) throw new NullPointerException("No task type.");
        if (c == null) throw new NullPointerException("No container.");
        if (i == null) throw new NullPointerException("No initializer.");
        
        //Instantiate through reflection, then link to c and i.
        InitTask task = null;
        try {
            Class<?> taskClass = (Class<?>) taskType;
            task = (InitTask) taskClass.getConstructor().newInstance();
            task.linkContainer(c);
            task.linkInitializer(i);
        } catch (InstantiationException ie) {
            throw new Exception(
                    "Couldn't instantiate initialization task.", ie);
        } catch (ClassCastException cce) {
            throw new Exception(
                    "Invalid initialization task: "+
                    taskType+" doesn't extends InitTask.");
        }
        return task;
    }

	/** Queue to order the tasks to be executed. */
	private List<InitTask>	processingQueue;
	
	/** The task that is currently processed. */
	private InitTask			currentTask;
	
	/** The tasks that have currently been executed. */
	private Stack<InitTask>	doneTasks;
	
	/** A reference to the singleton {@link Container}. */
	private Container container;
	 

    /**
     * Only for the benefit of subclasses that want to modify the
     * {@link #initList}.
     * This should only be done for the purpose of testing.
     */
    protected Initializer() {}
    
	/**
	 * Creates a new instance.
	 * The {@link Container} is the only class that can possibly create this
	 * object. In fact, the {@link Container} is the only class to have
	 * a reference to the singleton {@link Container}.
	 * 
	 * @param c	A reference to the singleton {@link Container}.
	 */
	public Initializer(Container c)
	{
		if (c == null) throw new NullPointerException();
		processingQueue = new ArrayList<InitTask>();
		doneTasks = new Stack<InitTask>();
		container = c;
	}
	
	/**
	 * Creates all needed initialization tasks, configures them, and queues
	 * them up for execution.
	 * This method has to be called before {@link #doInit()}.
     * 
     * @throws StartupException If an error occurs while configuring an
     *                          initialization task.
	 */
	public void configure()
        throws Exception
	{
		InitTask task;
        Iterator<Class<?>> type = initList.iterator();
        while (type.hasNext()) {
            task = createInitTask(type.next(), container, this); 
            task.configure();
            processingQueue.add(task);
        }
	}
	
	/**
	 * Performs the initialization procedure.
	 * 
	 * @throws StartupException	If an error occurs while executing an
	 * 							initialization task.
	 */
	public void doInit()
		throws Exception
	{
		Iterator<InitTask> i = processingQueue.iterator();
		
		//Execute all pending tasks. 
		while (i.hasNext()) {
			currentTask = i.next();
			currentTask.execute();
			doneTasks.push(currentTask);  //For later rollback if needed.
		}
	}
	
	/**
	 * Rolls back all tasks that have been executed at the time this method
	 * is invoked.
	 */
	public void rollback()
	{
		Iterator<InitTask> i = doneTasks.iterator();
		while (i.hasNext())
			i.next().rollback();
	}
	
}
