package example.env.init;

import example.env.Container;

abstract class InitTask {

	/** Reference to the singleton {@link Container}. */
	protected Container		container;
    
    /** Reference to the command processor. */
    protected Initializer   initializer;
	
    
	/**
     * Links this object to the {@link Container}.
	 * Called by the reflection code in the {@link Initializer} to set
     * the reference to the {@link Container} when the object is created.
	 * 
	 * @param c	Reference to the singleton {@link Container}.
	 */
	void linkContainer(Container c) { container = c; }
    
    /**
     * Links this object to the {@link Initializer}.
     * Called by the reflection code in the {@link Initializer} to set
     * the reference to the {@link Initializer} when the object is created.
     * 
     * @param i Reference to the {@link Initializer}.
     */
    void linkInitializer(Initializer i) { initializer = i; }
	
	/**
	 * Returns the name of this task.
	 * The returned string should be something meaningful to the user, as
	 * it will be displayed on the splash screen.
	 * This method is called after {@link #configure()}, but before 
	 * {@link #execute()}. 
	 * 
	 * @return	See above.
	 */
	abstract String getName();
	
	/**
	 * Prepare the task for execution.
	 * This method is called before {@link #execute()}. 
	 *
	 */
	abstract void configure();
	
	/**
	 * Carries out the initialization task.
	 * 
	 * @throws StartupException	If an error occurs.
	 */
	abstract void execute() throws Exception;
	
	/**
	 * Rolls back the initialization task.
	 * This method is typically implemented by those tasks that require
	 * to be undone if an error occurs during the initialization procedure
     * &#151; this allows for the container to exit gracefully.
	 */
	abstract void rollback();
}
