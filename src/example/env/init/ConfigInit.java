package example.env.init;

import example.env.Lookup;
import example.env.config.ConfigException;
import example.env.config.Registry;
import example.env.config.RegistryFactory;

/**
 * 
 * Fills up the {@link Container}'s registry with the entries from its
 * configuration file.
 *
 */
public final class ConfigInit 
	extends InitTask {

	@Override
	String getName() {
		return "Loading configuration";
	}

	@Override
	void execute() throws Exception {
		String file = container.getConfigFileRelative();
		Registry reg = container.getRegistry();
		try {
			RegistryFactory.fillFromFile(file, reg);
		} catch (ConfigException ce) {
			throw new Exception("Unable to load Container configuration", ce);
		}
		//Load the reference file
		String ref = (String) container.getRegistry().lookup(Lookup.REFERENCE_DATA);
		String refFile = container.getConfigFileRelative(ref);
		try {
			RegistryFactory.fillReferenceData(refFile, reg);
		} catch (ConfigException ce) {
			throw new Exception("Unable to load reference data", ce);
		}
	}

	@Override
	void configure() {}

	@Override
	void rollback() {}

}
