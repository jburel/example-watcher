package example.env.init;

import example.env.ui.UIFactory;
import example.env.ui.WatcherScreen;

public class WatcherScreenInit
	extends InitTask
{

	@Override
	String getName() {
		return "Initialize Watcher UI";
	}

	@Override
	void configure() {}

	@Override
	void execute() throws Exception {
		WatcherScreen screen = UIFactory.createScreen(container.getRegistry());
		screen.activate();
		
	}

	@Override
	void rollback() {}
}
