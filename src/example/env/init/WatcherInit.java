package example.env.init;

import example.env.config.Registry;
import example.env.config.RegistryFactory;
import example.env.svc.Watcher;
import example.env.svc.WatcherFactory;

public final class WatcherInit
	extends InitTask
{

	@Override
	String getName() {
		return "Initialize Directory Watcher";
	}

	@Override
	void configure() {}

	@Override
	void execute() throws Exception {
		Registry reg = container.getRegistry();
		Watcher watcher = WatcherFactory.makeNew(container);
		RegistryFactory.linkWatcher(watcher, reg);
	}

	@Override
	void rollback() {
		// TODO Auto-generated method stub
		
	}

}
