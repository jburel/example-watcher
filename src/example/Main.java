package example;


import example.env.Container;

public class Main {

	/**
	 * Main method.
	 * 
	 * @param args	Optional configuration file and path to the installation 
	 * 				directory. If not specified, then the user directory is 
	 * 				assumed and the <code>container.xml</code> is used.
	 */
	public static void main(String[] args) 
	{
		String homeDir = "";
		String configFile = null;
		if (args.length > 0) configFile = args[0];
		if (args.length > 1) homeDir = args[1];
		Container.startup(homeDir, configFile);
	}
}
