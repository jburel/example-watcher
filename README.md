Example project
---------------

* Watch a directory
* Read the file(s) when added to a specified directory
* Generate an output file and add it to another directory
* Input and output directories are specified using the config.xml file in the config folder
* ReferenceData.xml is used to set the emissions and value factors in the config folder

Improvement could be made to include for example a logger service using either the console or sls4j.
In order to have components communicating with UI, an event bus could be introduced e.g. using guava.

Date value was kept as string since the code assumes that the data will be collected at exactly the same time
for every generator. If this is not that case, using Java Date should introduce to support small variation in time.

Possible concurrency issues have not been looked into if a file with the same name is dropped at exactly the same time
in the folder and the parsing takes time. This was out of the scope of that exercise.


This code has been developed on MacOS X version 10.14.6 using Java 11.
The code can be run using an IDE e.g. Eclipse.
No build system provided.
No external libraries used.
